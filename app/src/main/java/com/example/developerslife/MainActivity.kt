package com.example.developerslife

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.google.android.material.floatingactionbutton.FloatingActionButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import javax.sql.DataSource
import com.bumptech.glide.request.target.Target
import java.util.*


class MainActivity : AppCompatActivity() {

    var currentPage = 0
    var arrayListOfData: ArrayList<Data> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        previousActionButton().isEnabled = false
        progressBar().isVisible = false
        repeatButton().isVisible = false
        loadAndPresentData()
    }

    fun onNextTap(view: View) {
        loadAndPresentData()
    }

    fun onPreviousTap(view: View) {
        if (currentPage != 0) {
            currentPage -= 1
        }
        if (arrayListOfData.size > currentPage) {
            val data = arrayListOfData.get(currentPage - 1)
            setGifUrl(data.gifURL)
            textView().text = data.description
        }
        if (currentPage == 1) {
            previousActionButton().isEnabled = false
        }
    }

    private fun loadAndPresentData() {
        imageView().setImageResource(android.R.color.transparent)
        textView().text = "Загрузка..."
        progressBar().isVisible = true
        repeatButton().isVisible = false

        if (arrayListOfData.size > currentPage) {
            val data = arrayListOfData.get(currentPage)
            setGifUrl(data.gifURL)
            textView().text = data.description
            currentPage += 1
            if (currentPage != 1) {
                previousActionButton().isEnabled = true
            }
        } else {
            NetworkService.create().randomJson().enqueue(
                object : Callback<Data> {
                    override fun onFailure(call: Call<Data>, t: Throwable) {
                        println(t.message)
                        repeatRequest()
                    }

                    override fun onResponse(
                        @NonNull call: Call<Data>,
                        @NonNull response: Response<Data>
                    ) {
                        println(response.body()?.description)
                        println(response.body()?.gifURL)
                        val data = response.body()
                        val description = data?.description
                        val gifURL = data?.gifURL
                        if (data != null && gifURL != null && description != null) {
                            currentPage += 1
                            arrayListOfData.add(data)
                            if (currentPage != 1) {
                                previousActionButton().isEnabled = true
                            }
                            setGifUrl(gifURL)
                            textView().text = description
                        } else {
                            repeatRequest()
                        }
                    }
                }
            )
        }
    }

    private fun repeatRequest() {
        progressBar().isVisible = false
        repeatButton().isVisible = true
        textView().text = "Что тот пошло не так. Попробуйте Повторить"
    }

    private fun previousActionButton(): FloatingActionButton {
        return findViewById<FloatingActionButton>(R.id.previousActionButton)
    }

    private fun nextActionButton(): FloatingActionButton {
        return findViewById<FloatingActionButton>(R.id.nextActionButton)
    }

    @SuppressLint("WrongViewCast")
    private fun repeatButton(): Button {
        return findViewById<Button>(R.id.repeatButton)
    }

    @SuppressLint("WrongViewCast")
    private fun textView(): TextView {
        return findViewById<TextView>(R.id.description)
    }

    @SuppressLint("WrongViewCast")
    private fun imageView(): ImageView {
        return findViewById<ImageView>(R.id.imageView)
    }

    @SuppressLint("WrongViewCast")
    private fun progressBar(): ProgressBar {
        return findViewById<ProgressBar>(R.id.progressBar)
    }

    private fun setGifUrl(string: String) {
        Glide
            .with(this)
            .asGif()
            .load(string)
            .listener(object : RequestListener<GifDrawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<GifDrawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    repeatRequest()
                    return false
                }

                override fun onResourceReady(
                    resource: GifDrawable?,
                    model: Any?,
                    target: Target<GifDrawable>?,
                    dataSource: com.bumptech.glide.load.DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    progressBar().isVisible = false
                    return false
                }
            })
            .into(imageView())
    }
}

interface ApiMethod {
    @GET("/random?json=true")
    fun randomJson(): Call<Data>
}

object NetworkService {
    fun create(): ApiMethod {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://developerslife.ru")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit.create(ApiMethod::class.java);
    }
}

data class Data(
    val description: String,
    val gifURL: String
)